"use strict";
const path = require("path");
// TODO move dequirk files to own folder

module.exports = [
	{
		// -------
		// DEQUIRK
		// -------
		entry: "./js/index.js",
		output: {
			filename: "dequirk.js",
			path: path.resolve(__dirname, "dist"),
		},
		module: {
			rules: [
				{
					test: /\.s[ac]ss$/iu,
					use: [
						// Creates `style` nodes from JS strings
						"style-loader",
						// Translates CSS into CommonJS
						"css-loader",
						// Compiles Sass to CSS
						"sass-loader",
					],
				},
			],
		},
		mode: "production",
	},
	{
		// -------
		// REQUIRK
		// -------
		entry: "./js/requirk/index.js",
		output: {
			filename: "requirk.js",
			path: path.resolve(__dirname, "dist"),
		},
		module: {
			rules: [
				{
					test: /\.s[ac]ss$/iu,
					use: [
						// Creates `style` nodes from JS strings
						"style-loader",
						// Translates CSS into CommonJS
						"css-loader",
						// Compiles Sass to CSS
						"sass-loader",
					],
				},
			],
		},
		mode: "production",
	},
	{
		// -------
		// GLOBAL
		// -------
		entry: "./js/global.js",
		output: {
			filename: "global.js",
			path: path.resolve(__dirname, "dist"),
		},
		module: {
			rules: [
				{
					test: /\.s[ac]ss$/iu,
					use: [
						// Creates `style` nodes from JS strings
						"style-loader",
						// Translates CSS into CommonJS
						"css-loader",
						// Compiles Sass to CSS
						"sass-loader",
					],
				},
			],
		},
		mode: "production",
	},
];
