// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

// Import stuff
const dequirk = {};
const req = require.context("./adventures", true, /^(.*\.(js$))[^.]*$/imu);
req.keys().forEach(key => {
	const out = req(key);
	dequirk[out.name] = out.data;
});

module.exports = dequirk;
