// Code by Yhvr <3
// yhvr.gitlab.io
// 413 AD: https://mspfa.com/?s=21074
// TODO finish up.

"use strict";

const Parser = require("../core.js");
const { fixCaps, ditch } = require("../prototype.js");
String.prototype.ditch = ditch;
String.prototype.fixCaps = fixCaps;

module.exports = {
	name: "413 AD",
	data: {
		CYGNIA: new Parser(
			{
				name: "Cygnia",
				listen: ["QC", "CYGNIA"],
				color: "#008282",
				handle: "quixoticConciliator [QC]",
			},
			text => text.replace(/2/gimu, "s").fixCaps()
		),
	},
};
