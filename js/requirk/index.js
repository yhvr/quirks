// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

const Line = require("./Line.js");
const Model = require("./Model.js");
const parseAliases = require("./alias.js");

window.Line = Line;
window.Model = Model;
window.alias = parseAliases;

window.app = new Vue({
	el: "#app",
	data: {
		quirks: {},
		quirk: "",
		text: "",
		tab: -10,
		update: 0,
		logName: "Pesterlog",
		toAdd: "",
		handler: "Plain",
		HTMLPreview: false,
		Model,
		Object,
		inp: parser => (parserDef[parser] || 0) > 0,
		inp2: parser => (parserDef[parser] || 0) > 1,
		inp3: parser => (parserDef[parser] || 0) > 2,
		// ------------------
		// NOTE TO SELF:
		// YOU CAN **NOT**
		// USE ARROW FUNCS!!!
		// ------------------
		handlePlain(text) {
			if (text === "") return "nothing yet DUMY....";
			let out = text.split("\n");
			const aliases = parseAliases(this.aliases());
			out = out.map(line => {
				let name = line.split(":")[0];
				let quirk = this.quirks[name];
				if (!quirk && this.quirks[aliases[name]])
					quirk = this.quirks[aliases[name]];
				if (quirk)
					return `[color=${quirk.color}]${name}:${quirk.parser.parse(
						line.split(":").slice(1).join(":")
					)}[/color]`;
				return line;
			});
			out = out.join("\n");
			for (const key in this.quirks) {
				const val = this.quirks[key];
				if (val.handle)
					out = out
						.split(val.handle)
						.join(`[color=${val.color}]${val.handle}[/color]`);
			}
			return `[spoiler open="Show ${this.logName}" close="Hide ${this.logName}"]
${out}
[/spoiler]`;
		},
		handleText(text) {
			if (text === "") return "nothing yet DUMY....";
			let out = text.split("\n");
			const aliases = parseAliases(this.aliases());
			out = out.map(line => {
				let name = line.split(":")[0];
				let quirk = this.quirks[name];
				if (!quirk && this.quirks[aliases[name]])
					quirk = this.quirks[aliases[name]];
				if (quirk)
					return `${name}:${quirk.parser.parse(
						line.split(":").slice(1).join(":")
					)}`;
				return line;
			});
			return out.join("\n");
		},
		handleHTML(text) {
			if (text === "") return "nothing yet DUMY....";
			let out = text.split("\n");
			const aliases = parseAliases(this.aliases());
			out = out.map(line => {
				let name = line.split(":")[0];
				let quirk = this.quirks[name];
				if (!quirk && this.quirks[aliases[name]])
					quirk = this.quirks[aliases[name]];
				if (quirk)
					return `<span style="color:${
						quirk.color
					}">${name}:${quirk.parser.parse(
						line.split(":").slice(1).join(":")
					)}</span>`;
				return line;
			});
			out = out.join("\n");
			for (const key in this.quirks) {
				const val = this.quirks[key];
				if (val.handle)
					out = out
						.split(val.handle)
						.join(`[color=${val.color}]${val.handle}[/color]`);
			}
			return `<pre>${out}</pre>`;
		},
		aliases() {
			let out = [];
			for (const key in app.quirks) {
				out.push(
					app.quirks[key].aliases.map(n => `${key}: ${n}`).join("\n")
				);
			}
			return out.join("\n");
		},
	},
	computed: {
		getHandler() {
			return this[`handle${this.handler}`];
		},
	},
});

window.addEventListener("keydown", e => {
	switch (e.key) {
		case "ArrowLeft":
			app.tab -= 10;
			// Breaks if you're on welcome menu, but that shouldnt affect much
			if (app.tab === -10) app.tab = 0;
			break;
		case "ArrowRight":
			app.tab += 10;
			if (app.tab === 40) app.tab = 0;
			break;
	}
});

// dialog: new MDCDialog(document.querySelector("#dialog2")),
// list: new MDCList(document.querySelector("#list2")),

// window.dialog = new MDCDialog(document.querySelector("#dialog"))
// window.list = new MDCList(document.querySelector(".mdc-dialog .mdc-list"));

// dialog.listen("MDCDialog:opened", () => {
//	list.layout();
// });

// app.dialog.listen("MDCDialog:opened", () => {
//	app.list.layout();
// });
