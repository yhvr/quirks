// Code by Yhvr <3
// yhvr.gitlab.io
// Vast Error: https://deconreconstruction.com/vasterror/
// TODO fix tazsia's color

"use strict";

const Parser = require("../core.js");
const { fixCaps, ditch } = require("../prototype.js");
String.prototype.ditch = ditch;
String.prototype.fixCaps = fixCaps;

module.exports = {
	name: "Vast Error",
	data: {
		SOVARA: new Parser(
			{
				name: "Sovara",
				listen: ["SA", "SOVARA"],
				color: "#7f0000",
				handle: "sanguineAllegory [SA]",
			},
			// Remove parenthesis
			text => text.ditch(1, 1)
		),
		DISMAS: new Parser(
			{
				name: "Dismas",
				listen: ["GD", "DISMAS"],
				color: "#c44000",
				handle: "gigantisDebilitation [GD]",
			},
			text =>
				// It's far from perfect, but it gets the job done
				// TODO make better?
				text
					.ditch(0, 4)
					.replace(/\/\\/gimu, "A")
					.replace(/\\\//gimu, "V")
		),
		ARCJEC: new Parser(
			{
				name: "Arcjec",
				listen: ["AH", "ARCJEC"],
				color: "#b95c00",
				handle: "animatedHumorist [AH]",
			},
			// Pretty easy!
			text => text.dicth(5, 5)
		),
		JENTHA: new Parser(
			{
				name: "Jentha",
				listen: ["FF", "JENTHA"],
				color: "#b49e18",
				handle: "furbishFacilitated [FF]",
			},
			text => {
				let out = text;
				"abcdefghijklmnopqrstuvwxyz"
					.split("")
					.forEach(
						n =>
							(out = out.replace(
								new RegExp(`(?<=\\b)${n} (?=${n})`, "gimu"),
								""
							))
					);
				return out;
			}
		),
		ELLSEE: new Parser(
			{
				name: "Ellsee",
				listen: ["EO", "ELLSEE"],
				color: "#6c8400",
				handle: "existereOracle [EO]",
			},
			text =>
				text
					.replace(/Σ/gimu, "e")
					.replace(/¿¿/gimu, "?")
					.replace(/¡¡/gimu, "!")
		),
		ALBION: new Parser(
			{
				name: "Albion",
				listen: ["DQ", "ALBION"],
				color: "#407d00",
				handle: "demiurgeQuantified [DQ]",
			},
			text => text.replace(/\*/gimu, " ")
		),
		SERPAZ: new Parser(
			{
				name: "Serpaz",
				listen: ["PD", "SERPAZ"],
				color: "#00a596",
				handle: "pliableDecadence [PD]",
			}
			// It's just font!
		),
		LAVIAN: new Parser(
			{
				name: "Lavian",
				listen: ["WA", "LAVIAN"],
				color: "#004696",
				handle: "windlessArtificer [WA]",
			},
			text => text.replace(/-/gimu, ".").replace(/Woof- /gimu, "")
		),
		OCCEUS: new Parser(
			{
				name: "Occeus",
				listen: ["ME", "OCCEUS"],
				color: "#00007f",
				handle: "macabreExude [ME]",
			},
			text => text.replace(/\.o\./gimu, "o").replace(/eye/gimu, "I")
		),
		TAZSIA: new Parser(
			{
				name: "Tazsia",
				listen: ["PO", "TAZ"],
				color: "#00722d",
				handle: "perniciousOverkill [PO]",
			},
			text => text.ditch(1, 1).replace(/\+/gimu, "t")
		),
		MURRIT: new Parser(
			{
				name: "Murrit",
				listen: ["UK", "MURRIT"],
				color: "#510049",
				handle: "unclaspedKahuna [UK]",
			},
			text => text.ditch(3, 1).replace(/#/gimu, "h")
		),
		CALDER: new Parser({
			name: "Calder",
			listen: ["GS", "CALDER"],
			color: "#a00078",
			handle: "grandioseSaturation [GS]",
		}),
	},
};
