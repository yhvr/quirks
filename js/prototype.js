// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

// -----------------------
// PROTOTYPE MODIFICATIONS
// -----------------------

// This capitalizes every first letter in a sentence.
String.prototype.fixCaps = function () {
	return this.toLowerCase()
		.split(". ")
		.map(n =>
			n
				.split("")
				.map((m, i) => (i === 0 ? m.toUpperCase() : m))
				.join("")
		)
		.join(". ");
};

// For removing prefixes and suffixes.
String.prototype.ditch = function (start, end) {
	return this.slice(start, 0 - end);
};

module.exports = String.prototype;