// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

const app = new Vue({
	el: "#app",
	data: {
		text: "Put your text here...",
		parse: require("./parser.js"),
	},
});

import { MDCSelect } from "@material/select";

const select = new MDCSelect(document.querySelector(".mdc-select"));

select.listen("MDCSelect:change", () => {
	console.log(select)
	app.adventure = select.value
});
