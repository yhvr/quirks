// Code by Yhvr <3
// yhvr.gitlab.io
// Homestuck: https://homestuck.com/
// TODO use String.prototype.ditch()?

"use strict";

const Parser = require("../core.js");
const { fixCaps, ditch } = require("../prototype.js");
String.prototype.ditch = ditch;
String.prototype.fixCaps = fixCaps;

module.exports = {
	name: "Homestuck",
	data: {
		ARADIA: new Parser(
			{
				name: "Aradia",
				listen: ["AA", "ARADIA"],
				color: "#a10000",
				handle: "apocalypseArisen [AA]",
			},
			// Makes sure it's not part of a number or emote
			text =>
				text
					.replace(/(?<![_\d])0(?![_\d])/gimu, "o")
					.replace(/(?<![_\d])00(?![_\d])/gimu, "oo")
		),
		TAVROS: new Parser(
			{
				name: "Tavros",
				listen: ["AT", "TAVROS"],
				color: "#a15000",
				handle: "adiosToreador [AT]",
			},
			text =>
				text
					// Remove horns from smileys
					.replace(/\}(?=:)/gimu, "")
					// Invert capitalization
					// this could be a one-liner
					// (map w/ tenary) but prettier
					// and ESLint didnt want to work together
					.split("")
					.map(n => {
						if (n === n.toLowerCase()) return n.toUpperCase();
						return n.toLowerCase();
					})
					.join("")
					// Ditch multiple commas
					.replace(/,{2,}/gimu, ",")
		),
		SOLLUX: new Parser(
			{
				name: "Sollux",
				listen: ["TA", "SOLLUX"],
				color: "#a1a100",
				handle: "twinArmageddons [TA]",
			},
			text =>
				text
					// 2 -> s
					.replace(/(?<!\d)2(?!\d)/gimu, "s")
					// 22 -> ss
					.replace(/(?<!\d)22(?!\d)/gimu, "ss")
					// Double i -> i
					.replace(/ii/gimu, "i")
					// Two -> To (could also be too but this works)
					.replace(/two/gimu, "to")
					// 0 -> o (blind/dead)
					.replace(/(?<!\d)0(?!\d)/gimu, "o")
		),
		KARKAT: new Parser(
			{
				name: "Karkat",
				listen: ["CG", "KARKAT"],
				color: "#626262",
				handle: "carcinoGenecist [CG]",
			},
			text => text.fixCaps()
		),
		NEPETA: new Parser(
			{
				name: "Nepeta",
				listen: ["AC", "NEPETA"],
				color: "#416600",
				handle: "arsenicCatnip [AC]",
			},
			// Get rid of ":33 <"
			// Replace 33 with ee
			text => text.substring(5).replace(/33/gimu, "ee")
		),
		KANAYA: new Parser(
			{
				name: "Kanaya",
				listen: ["GA", "KANAYA"],
				color: "#008141",
				handle: "grimAuxillatrix [GA]",
			},
			text => text.fixCaps()
		),
		TEREZI: new Parser(
			{
				name: "Terezi",
				listen: ["GC", "TEREZI"],
				color: "#008282",
				handle: "gallowsCalibrator [GC]",
			},
			text =>
				text
					.fixCaps()
					.replace(/(?<![025-9])4(?![025-9])/gimu, "a")
					.replace(/(?<![025-9])3(?![025-9])/gimu, "e")
					.replace(/(?<![025-9])1(?![025-9])/gimu, "i")
					// Remove horns from smileys
					.replace(/>(?=:)/gimu, "")
		),
		VRISKA: new Parser({
			name: "Vriska",
			listen: ["AG", "VRISKA"],
			color: "#005682",
			handle: "arachnidsGrip [AG]",
		}),
		EQUIUS: new Parser(
			{
				name: "Equius",
				listen: ["CT", "EQUIUS"],
				color: "#000056",
				handle: "centaursTesticle [CT]",
			},
			text =>
				text
					// Get rid of "D -->""
					.substring(5)
					.replace(/b100(?=\b)/gimu, "blue")
					.replace(/(?<![2-9])1(?![2-9])/gimu, "l")
					.replace(/(?<![2-9])0(?![2-9])/gimu, "o")
		),
		GAMZEE: new Parser(
			{
				name: "Gamzee",
				listen: ["TC", "GAMZEE"],
				color: "#2b0057",
				handle: "terminallyCapricious [TC]",
			},
			text => text.fixCaps().replace(/ motherfuckin('|g)?/gimu, "")
		),
		ERIDAN: new Parser(
			{
				name: "Eridan",
				listen: ["CA", "ERIDAN"],
				color: "#6a006a",
				handle: "caligulasAquarium [CA]",
			},
			text =>
				text
					// Un-double
					.replace(/ww/gimu, "w")
					.replace(/vv/gimu, "v")
					// "-in" -> "-ing"
					.replace(/in(?=\b)/gimu, "ing")
		),
		// TODO: Fishy troll name?
		FEFERI: new Parser(
			{
				name: "Feferi",
				listen: ["CC", "FEFERI"],
				color: "#77003c",
				handle: "cuttlefishCuller [CC]",
			},
			// Remove trident handles
			text =>
				text
					.replace(/-+(?=E)/gimu, "")
					// Capital )(
					.replace(/\)\((?=[A-Z])/gimu, "H")
					// Lowercase )(
					.replace(/\)\(/gimu, "h")
		),
	},
};

const AG = new Parser(
	{
		name: "Vriska",
		listen: ["AG", "VRISKA"],
	},
	text =>
		text
			// Vriska.
			// TODO Merge regexes?
			.replace(/t8m/gimu, "tatem")
			.replace(/(?<=\b)8(?=[aeiouryl])/gimu, "b")
			.replace(/do8ng/gimu, "doing")
			.replace(/l8y/gimu, "lay")
			.replace(/g8me/gimu, "game")
			.replace(/w8s/gimu, "was")
			.replace(/h8(?=[nt])/gimu, "ha")
			.replace(/h8l/gimu, "hol")
			.replace(/i8l/gimu, "ibl")
			.replace(/i8t/gimu, "iot")
			.replace(/c8u/gimu, "cou")
			.replace(/d8n/gimu, "don")
			.replace(/n8w/gimu, "now")
			.replace(/t8p/gimu, "top")
			.replace(/b8d/gimu, "bad")
			.replace(/gr8/gimu, "great")
			.replace(/qu8te/gimu, "quite")
			.replace(/fin8/gimu, "fine")
			.replace(/(<=\b)8ye/gimu, "bye")
			.replace(/8/gimu, "b")
);
