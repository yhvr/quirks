"use strict";

module.exports = class {
	constructor(who = "NOBODY", content = "Nothing", meta) {
		this.who = who;
		if (who === "LOG") {
			this.to = meta.to || "Nobody";
			this.from = meta.from || "Nobody";
			this.template = content;
		} else {
			this.content = content;
			this.parser = meta;
		}
	}

	toHTML() {
		if (this.who === "LOG")
			return this.template
				.replace(/\{from\}/gimu, `${this.from}`)
				.replace(/\{to\}/gimu, `${this.to}`);
		return `TODO!`;
	}

	toText() {
		if (this.who === "LOG")
			return this.template
				.replace(/\{from\}/gimu, `${this.from}`)
				.replace(/\{to\}/gimu, `${this.to}`);
		return `${this.who}: ${this.parser.parse(this.content)}`;
	}
};
