// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

import "../main.scss";

import { MDCDrawer } from "@material/drawer";
import { MDCTopAppBar } from "@material/top-app-bar";
import { MDCRipple } from "@material/ripple";

window.drawer = MDCDrawer.attachTo(document.querySelector(".mdc-drawer"));
const topAppBar = MDCTopAppBar.attachTo(document.getElementById("app-bar"));
topAppBar.setScrollTarget(document.getElementById("app"));
topAppBar.listen("MDCTopAppBar:nav", () => {
	drawer.open = !drawer.open;
});
const body = document.querySelector("#app");

window.drawer.addEventListener("click", event => {
	body.querySelector("input, button").focus();
});

document.body.addEventListener("MDCDrawer:closed", () => {
	body.querySelector("input, button").focus();
});

document.querySelectorAll(".ripple").forEach(i => new MDCRipple(i));
document.querySelectorAll("button").forEach(i => new MDCRipple(i));
