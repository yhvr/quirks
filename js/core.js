"use strict";

module.exports = class Parser {
	constructor(data, parse) {
		this.name = data.name || "No Name";
		this.listen =
			data.listen ||
			// In case there isn't a name(s) specified
			this.name
				.split(" ")
				.map(n => n[0])
				.join("");
		if (this.listen instanceof String) this.listen = [this.listen];
		this.parser = parse || (n => n);
		this.color = data.color || "#000000";
		this.handle = data.handle
			? data.handle.replace(/\[/gimu, "\\[").replace(/\]/gimu, "\\]")
			: "noHandle \\[NH\\]";
	}
};
