# Parser Guide

TODO: make this file html
This is a guide to all the types of parsers in requirk.

## `uppercase { r }`

Turn all 1-char matches of the RegExp `r` with flags `gumi` into their upparcase counterpart.

## `lowercase { r }`

Turn all 1-char matches of the RegExp `r` with flags `gumi` into their lowercase counterpart.

## `replace { r } [ t ]`

Replace all matches of the RegExp `r` with flags `gumi` or `f` with `t`, or just remove them if `t` doesn't exist.

## `prefix { t }`

Prepend `t` to the message content.

## `suffix { t }`

Append `t` to the message content.

## `eval { f }`

Evaluate the JavaScript expression `f` with a header of `text =>` and replace the text with that. e.g, `text.split("").map(n=>n=="a"?"":n).join("")`
