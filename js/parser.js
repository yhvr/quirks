// Code by Yhvr <3
// yhvr.gitlab.io
// Where the actual stuff is done

"use strict";

// Turn the easy backend... (`dequirk`)
// into an easy frontend (`adventures`)
const dequirk = require("./adventures.js");
const adventures = {};

// Iterate over all adventures
for (const key in dequirk) {
	// Init adventures sub-object
	adventures[key] = {};
	// Iterate over all quirks in an adventure
	for (const key2 in dequirk[key]) {
		const data = dequirk[key][key2];
		const listens = data.listen;
		// Iterate over the aliases in a
		// quirk in an adventure
		for (const key3 in listens)
			adventures[key][listens[key3]] = {
				color: data.color,
				handle: data.handle,
				sanitizedHandle: data.handle
					.replace(/\\\[/gimu, "[")
					.replace(/\\\]/gimu, "]"),
				parse: data.parser,
			};
	}
}

module.exports = function parse(text, adventure = "Homestuck") {
	const parsers = Object.keys(adventures[adventure]);
	let output = text;
	parsers.forEach(
		n =>
			(output = output.replace(
				new RegExp(adventures[adventure][n].handle, "gumi"),
				`<span style="color:${adventures[adventure][n].color}">${adventures[adventure][n].sanitizedHandle}</span>`
			))
	);
	return output
		.split("\n")
		.map(line => {
			const name = line.split(": ")[0];
			if (parsers.includes(name))
				return `<span style="color:${
					adventures[adventure][name].color
				}">${name}: ${adventures[adventure][name].parse(
					line.substring(name.length + 2)
				)}</span>`;
			return line;
		})
		.join("\n");
};
