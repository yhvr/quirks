/* eslint-disable no-eval */

"use strict";

const parsers = {
	uppercase: (text, regex) =>
		text
			.split("")
			.map(n =>
				n.match(new RegExp(regex, "gimu")) ? n.toUpperCase() : n
			)
			.join(""),
	lowercase: (text, regex) =>
		text
			.split("")
			.map(n =>
				n.match(new RegExp(regex, "gimu")) ? n.toLowerCase() : n
			)
			.join(""),
	prefix: (text, pre) => `${pre}${text}`,
	suffix: (text, suf) => `${text}${suf}`,
	eval: (text, code) => eval(`(text => ${code})`)(text),
	replace: (text, regex, repl) =>
		text.replace(new RegExp(regex, "gumi"), repl),
	kanaya: text =>
		text
			.split(" ")
			.map(n =>
				n
					.split("")
					.map((o, i) => (i === 0 ? o.toUpperCase() : o))
					.join("")
			)
			.join(" "),
};

const parserDef = {
	uppercase: 1,
	lowercase: 1,
	eval: 1,
	replace: 2,
	prefix: 1,
	suffix: 1,
	kanaya: 0,
};

window.parsers = parsers;
window.parserDef = parserDef;

module.exports = class {
	constructor(a) {
		this.parsers = [];
		// If it's importing!
		if (a) {
			this.parsers = JSON.parse(a).map();
		}
	}

	addParser() {
		let temp = this.parsers;
		this.parsers.push({
			type: "uppercase",
			content: "",
			content2: "",
			content3: "",
		});
		let out = this;
		this.parsers = temp;
		return out;
	}

	removeParser(index) {
		let temp = this.parsers;
		this.parsers.splice(index, 1);
		let out = this;
		this.parsers = temp;
		return out;
	}

	export() {
		return JSON.stringify(
			this.parsers.map(p => {
				return {
					type: p.type,
					con: [p.content, p.content2, p.content3],
				};
			})
		);
	}

	parse(text) {
		let out = text;
		this.parsers.forEach(parser => {
			if (parsers[parser.type]) {
				out = parsers[parser.type](
					out,
					parser.content,
					parser.content2,
					parser.content3
				);
			}
		});
		return out;
	}
};
