"use strict";

module.exports = list => {
	let obj = {};
	list.split("\n")
		.map(n => n.split(": "))
		.forEach(n => (obj[n[1]] = n[0]));
	return obj;
};
