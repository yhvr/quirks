// Code by Yhvr <3
// yhvr.gitlab.io

"use strict";

const Parser = require("./Parser.js");

module.exports = class {
	constructor() {
		this.parser = new Parser();
		this.aliases = [];
		this.color = "#000000";
		this.handle = "";
	}
};
